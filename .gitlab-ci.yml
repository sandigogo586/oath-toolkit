stages:
 - build
 - test
 - deploy

Debian:
  image: debian:latest
  stage: build
  before_script:
    - apt-get update -qq
    - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq make git autoconf automake libtool bison libpam0g-dev libxmlsec1-dev libxml2-utils help2man gtk-doc-tools dblatex valgrind gengetopt libxml2-dbg datefudge
  script:
  - echo "--suppressions=$(pwd)/libpskc/tests/libpskc.supp" >> ~/.valgrindrc
  - make bootstrap
  - make syntax-check
  - make V=1 || (find . -name test-suite.log -exec cat {} +; exit 1)
  - make dist
  - sha224sum oath-toolkit-*.tar.gz
  artifacts:
    expire_in: 2 weeks
    paths:
      - oath-toolkit-*.tar.gz

Ubuntu-coverage:
  image: ubuntu:devel
  stage: build
  before_script:
    - apt-get update -qq
    - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq make git autoconf automake libtool bison libpam0g-dev libxmlsec1-dev libxml2-utils help2man gtk-doc-tools valgrind gengetopt datefudge lcov
  script:
  - echo "--suppressions=$(pwd)/libpskc/tests/libpskc.supp" >> ~/.valgrindrc
  - make autoreconf
  - ./configure --enable-gcc-warnings --enable-root-tests --enable-valgrind-tests CFLAGS="-g --coverage" || (cat config.log; exit 1)
  - lcov --directory . --zerocounters
  - make check V=1 || (find . -name test-suite.log -exec cat {} +; exit 1)
  - mkdir coverage
  - lcov --directory . --output-file coverage/oath-toolkit.info --capture
  - lcov --remove coverage/oath-toolkit.info '/usr/include/*' '*/liboath/gl/*' '*/liboath/tests/*' '*/libpskc/gl/*' '*/libpskc/tests/*' '*/oathtool/gl/*' '*/pam_oath/tests/*' '*/pskctool/gl/*' -o coverage/oath-toolkit_filtered.info
  - genhtml --output-directory coverage coverage/oath-toolkit_filtered.info --highlight --frames --legend --title "OATH Toolkit"
  artifacts:
    when: on_success
    paths:
      - coverage
  only:
    - master

Fedora-clanganalyzer:
  image: fedora:latest
  stage: build
  before_script:
  - yum -y install git make clang clang-analyzer diffutils file autoconf automake libtool bison libxml2-devel help2man gtk-doc gengetopt datefudge libxml2-devel libtool-ltdl-devel xmlsec1-openssl-devel
  script:
  - make autoreconf
  - scan-build ./configure --with-xmlsec-crypto-engine=openssl || (cat config.log; exit 1)
  - scan-build -o clang-analyzer make V=1
  artifacts:
    when: on_success
    paths:
      - clang-analyzer
  only:
    - master

pages:
  stage: deploy
  needs: ["Ubuntu-coverage", "Fedora-clanganalyzer"]
  script:
    - mkdir public
    - mv coverage/ public/
    - mv clang-analyzer/* public/clang-analyzer
  artifacts:
    paths:
    - public
    expire_in: 30 days
  only:
  - master

Ubuntu-xmlsec:
  image: ubuntu:latest
  stage: test
  needs: [Debian]
  before_script:
    - apt-get update -qq
    - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq make gcc pkg-config libxmlsec1-dev libxml2-utils datefudge
  script:
  - tar xfa oath-toolkit-*.tar.gz
  - cd `ls -d oath-toolkit-* | grep -v tar.gz`
  - mkdir b
  - cd b
  - ../configure --enable-gcc-warnings --disable-valgrind-tests || (cat config.log; exit 1)
  - make check V=1 || (find . -name test-suite.log -exec cat {} +; exit 1)

CentOS:
  image: centos:latest
  stage: test
  needs: [Debian]
  before_script:
  - yum -y install make gcc diffutils file
  script:
  - tar xfa oath-toolkit-*.tar.gz
  - cd `ls -d oath-toolkit-* | grep -v tar.gz`
  - ./configure --disable-pskc --enable-gcc-warnings --disable-valgrind-tests || (cat config.log; exit 1)
  - make check V=1 || (find . -name test-suite.log -exec cat {} +; exit 1)

CentOS-xmlsec:
  image: centos:latest
  stage: test
  needs: [Debian]
  before_script:
  - yum -y install make gcc diffutils file
  - yum -y install libxml2-devel libtool-ltdl-devel
  - dnf -y --enablerepo=PowerTools install xmlsec1-gnutls xmlsec1-gnutls-devel
  script:
  - tar xfa oath-toolkit-*.tar.gz
  - cd `ls -d oath-toolkit-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings --disable-valgrind-tests --with-xmlsec-crypto-engine=gnutls || (cat config.log; exit 1)
  - make check V=1 || (find . -name test-suite.log -exec cat {} +; exit 1)

Alpine:
  image: alpine:latest
  stage: test
  needs: [Debian]
  before_script:
  - echo "ipv6" >> /etc/modules
  - apk update
  - apk add build-base datefudge
  script:
  - tar xfz oath-toolkit-*.tar.gz
  - cd `ls -d oath-toolkit-* | grep -v tar.gz`
  - ./configure --disable-pskc --enable-gcc-warnings || (cat config.log; exit 1)
  - sed -i -e 's,  /\* Outlandishly-long.*,#if 0,;s,  return 0;,#endif\nreturn 0;,' ./oathtool/gl/tests/test-parse-datetime.c
  - make check V=1 || (find . -name test-suite.log -exec cat {} +; exit 1)

ArchLinux:
  image: archlinux:latest
  stage: test
  needs: [Debian]
  before_script:
  - pacman -Sy --noconfirm make gcc diffutils file
  script:
  - tar xfz oath-toolkit-*.tar.gz
  - cd `ls -d oath-toolkit-* | grep -v tar.gz`
  - mkdir b
  - cd b
  - ../configure --disable-pskc --enable-gcc-warnings || (cat config.log; exit 1)
  - make check V=1 || (find . -name test-suite.log -exec cat {} +; exit 1)

ArchLinux-xmlsec:
  image: archlinux:latest
  stage: test
  needs: [Debian]
  before_script:
  - pacman -Sy --noconfirm make gcc diffutils file xmlsec pkg-config
  script:
  - tar xfz oath-toolkit-*.tar.gz
  - cd `ls -d oath-toolkit-* | grep -v tar.gz`
  - mkdir b
  - cd b
  - ../configure --enable-gcc-warnings || (cat config.log; exit 1)
  - make check V=1 || (find . -name test-suite.log -exec cat {} +; exit 1)

# Mingw64:
#   image: debian:latest
#   stage: test
#   needs: [Debian]
#   before_script:
#     - apt-get update -qq
#     - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq make mingw-w64 wine wine64 binfmt-support wine-binfmt
#     - update-binfmts --enable wine
#     - update-binfmts --display
#     - mount
#     - find /proc/sys/fs/binfmt_misc
#   script:
#   - tar xfa oath-toolkit-*.tar.gz
#   - cd `ls -d oath-toolkit-* | grep -v tar.gz`
#   - ./configure --host=x86_64-w64-mingw32
#   - make V=1
#   - make -C liboath/tests check || (cat liboath/tests/test-suite.log +; exit 1)
#   - make -C oathtool/tests check || (cat oathtool/tests/test-suite.log +; exit 1)

# Mingw32:
#   image: debian:latest
#   stage: test
#   needs: [Debian]
#   before_script:
#     - dpkg --add-architecture i386
#     - apt-get update -qq
#     - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq make mingw-w64 wine wine32 libwine libwine:i386 binfmt-support wine-binfmt
#     - update-binfmts --enable wine
#     - update-binfmts --display
#     - mount
#     - find /proc/sys/fs/binfmt_misc
#   script:
#   - tar xfa oath-toolkit-*.tar.gz
#   - cd `ls -d oath-toolkit-* | grep -v tar.gz`
#   - ./configure --host=i686-w64-mingw32
#   - make V=1
#   - make -C liboath/tests check || (cat liboath/tests/test-suite.log +; exit 1)
#   - make -C oathtool/tests check || (cat oathtool/tests/test-suite.log +; exit 1)
